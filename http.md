# http 

超文本传输协议；

针对浏览器和服务器进行通信的
1. 浏览器主动向服务器发请求
2. 服务器接受请求，作出响应
3. 作出响应自动断开协议

哪些情况会发起请求？

1. 浏览器url地址按回车,一般返回document
2. <script src=""></script>
3. <link href=""></link>
4. <img src="" />
5. <media src=""></media>  <audio src=""></audio>
6. css font-family:''
以上这些文件（静态资源）请求不受同源策略限制
7. fetch 和 XHR（XMLHttpRequest）
受同源策略限制，同协议+同域名+同端口, 违反同源策略需要处理跨域问题

1. 解决跨域问题
- 反向代理
  - 开发环境
    - webpack-devServer proxy
  - 生产环境
    - nginx proxy_pass

- jsonp
  - 通过script标签的src属性来实现

- cors 跨域资源共享
  - 服务器配置headers头，实现资源响应
  - 简单请求/非简单请求（提前发起预检请求，请求方式option）

- postmessage 
  - 主窗口和子窗口进行通信


  ## headers

  - Content-Type: 文件类型，
  post请求，发送json格式数据是`application/json`
  post请求，发送FormData对象是`multpart/form-data` FormData对象的value可以是字符串也可以是file对象
  get请求，通过url地址拼接参数传递queryString， `application/x-www-form-urlencoded`

  - Cookie 浏览器自动携带
  - User-Agent 浏览器信息