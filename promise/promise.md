## promise

promise解决了异步编程回调嵌套，回调地狱的问题

promise是一个包裹异步操作的一个容器，它有3种状态：padding，reject，resolve三种状态，可以从padding到成功，或者padding到失败，状态一旦发生变化，会执行对应 then 放和catch方法

promise怎么实现链式调用。不断返回新的promise实例实现链式调用


## 浏览器事件队列/eventloop/解析过程

js脚本是单线程解析，为了不阻塞脚本运行，先执行同步脚本，碰到异步脚本放入事件队列等待执行，等所有的同步脚本执行完成，去事件队列中查找异步任务执行；
异步任务分为两种，宏任务和微任务；
宏任务是浏览器发起的（定时器，network，事件，dom渲染）
微任务（promise、MutationObserver）
先执行微任务在执行宏任务


## 手写实现promise

```
const PADDING = 'PADDING';
const FULLFILD = 'FULLFILD';
const REJECT = 'REJECT';
class Promise {
  constructor(callback){
    this.status = PADDING;
    this.resolve = function(data){ //异步的
      this.status = FULLFILD;
      this.resolveCk(data);
    }
    this.reject = function(error){ // 异步
      this.status = REJECT;
      if(this.rejectCk){
        this.rejectCk(error)
        return
      }
      if(this.catchCk) {
        this.catchCk(error)
      }
    }
    callback(this.resolve, this.reject)
  }
  then(resolveCk, rejectCk){
    this.resolveCk = resolveCk;
    this.rejectCk = rejectCk;
    return new Promise()
  }
  catch(ck){
    this.catchCk = ck;
    return new Promise()
  }
}

new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(1)
  })
}).then(res => {
  console.log(res);
})
``` 

## 手写实现Promise.all
多个异步请求同时执行

## 手写实现Promise.race

## Promise.all 和 Promise.race 的区别
