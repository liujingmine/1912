const getkey = (time) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      Math.random() > 0.5 ? resolve('success'+ '_' + time) : reject('error');
    }, time)
  })
}


Promise.myall = function(promiseArr) {
  if(!Array.isArray(promiseArr)){
    return new Error('promise all 接受一个数组')
  }
  return new Promise((resolve, reject) => {
    let resArr = [];
    let count = 0;
    for( let i = 0; i < promiseArr.length; i++ ) {
      promiseArr[i].then(res => {
        resArr[i] = {
          type: 'success',
          value: res
        };
        count++;
      }).catch(error => {
        count++;
        resArr[i] = {
          type: 'error',
          value: error
        };
      }).finally(() => {
        if(count === promiseArr.length){
          resolve(resArr);
        }
      })
    }
  })
}

const deley = (time) => new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(time);
  },time)
})

const xhr = ({method="get", url=""}) => new Promise((resolve, reject) => {
  const _xhr = new XMLHttpRequest();
  _xhr.open(method, url);
  _xhr.onreadystatechange = function(){
    if(_xhr.readyState === 4){
      if(_xhr.status >= 200 && _xhr.status < 300 || _xhr.status === 304) {
        resolve(_xhr.response)
      } else  {
        reject(_xhr.response)
      }
    } else {
      console.log('loading...')
    }
  }
  _xhr.send();
}) 


// Promise.race([deley(10000), deley(500)]).then(res => {
//   console.log(res);
// })
Promise.any([getkey(1000), getkey(500), getkey(300)]).then(res => {
  console.log('then---', res);
}).catch(error => {
  console.log('error----',error);
})