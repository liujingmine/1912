const getkey = (ck) => {
  setTimeout(() => {
    console.log('setTimeout')
    const key = Math.random() > 0.2 ? 'success' : 'error';
    ck(key);
  }, 1000)
}

// 得到key

getkey(function(res){
  console.log('第一关')
  if(res === 'success'){
    getkey(function(res){
      console.log('第二关')
    })
  }
})
console.log('start')