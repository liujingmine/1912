## useState
- 作用： 让函数组件拥有自己的状态
- 语法： useState(初始状态)
- 返回值： [当前状态, 设置状态的函数]
调用设置状态的函数会让函数组件从新执行


## useEffect
- 作用：定义函数副作用，特定时间执行的函数，类似class组件的生命周期
- 语法：useEffect(callback:function, ?[])
- 第二参数是可选参数，
不传：组件创建组件更新都会执行；（componentDidMount + componentDidUpdate）
传空数组：组件初始创建dom节点加载完成执行一次（componentDidMount）
传非空数组：数组中放入要监听的数据，初始创建和数据更新时会执行 （componentDidMount + shoulComponentUpdate(nextProps, nextState) return true + componentDidUpdate）
- 第一个函数返回的函数替代componentWillUnmount

## useRef
- 作用：定义组件常量，在组件更新时保留之前状态；
- 语法：{current: 当前值} = useRef(初始值)


## class组件生命周期
- 初始化组件
1. constrcutor 构造函数，接受props，修改this super(props) 完成组件继承,继承过来 setState，设置初始state 执行一次
2. render 生成虚拟dom
3. componentDidMount dom节点初始加载完成 执行一次
- 更新组件 
shoulComponentUpdate(nextProps, nextState){
  this.props nextProps 对比
  this.state nextState 对比
  return true 接着执行下面的生命周期
  return false 不执行下面操作
}
render 进行diff
componentDidUpdate 组件更新完成，dom更新完成
- 卸载组件
componentWillUnmount 移除全局操作，避免造成内存泄漏
- [捕获错误](https://zh-hans.reactjs.org/docs/react-component.html#static-getderivedstatefromerror)
componentDidCatch 配合 static getDerivedStateFromError(error)

class A extends React.Component {
  constrcutor(props){
    super(props);
    this.state = {
      title: '标题'
    }
  }
  render(){

  }
}

class A extends React.Component {
  title = "标题";
  render(){
    return (
      <div></div>
    )
  }
};

