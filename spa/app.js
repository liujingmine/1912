const http = require('http');
const fs = require('fs');
http.createServer((req, res) => {
  console.log('请求路径为:', req.url , '----------');
  if(req.url === '/'){
    res.end(fs.readFileSync('./index.html'))
    return;
  }

  if(req.url === '/list') {
    res.end(fs.readFileSync('./list.html'));
    return;
  }

  if(req.url === '/style.css') {
    res.end(fs.readFileSync('./style.css'));
    return;
  }
  
}).listen(3000);