### spa
单页面应用

1. 在页面路由跳转的时候不向服务器发请求

  - 改变url#后面的内容， 这种路由模式称为hash
  - 通过window.history.pushState进行跳转, 这种路由模式称为history
