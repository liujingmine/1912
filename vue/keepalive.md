## 作用
缓存组件实例

## 用法
https://vuejs.org/guide/built-ins/keep-alive.html#basic-usage




## 生命周期

第一次组件创建
beforeCreate
created
beforeMount
mounted
activted (包裹keepAlive的组件才会执行)

更新
beforeUpdate
updated
$nextTick

组件离开
beforeUnmount
mounted
deactivated (包裹keepAlive的组件才会执行)