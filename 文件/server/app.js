const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const body = require('koa-body');
const fs = require('fs');
const xlsx = require('node-xlsx');
// app.use();
app.use(router.routes(),router.allowedMethods())

const saveFile = (readFile, writeFile) => {
  return new Promise((resolve) => {
    readFile.pipe(writeFile);
    readFile.on('end', () => {
      resolve();
    })
  })
}
const clomus = [
  {
    name: "订单编号",
    key: "order"
  },
  {
    name: "下单时间",
    key: "createTime"
  },
  {
    name: "收件人",
    key: "pullUserName"
  },
  {
    name: "手机",
    key: "phone"
  },
  {
    name: "收货地址",
    key: "address"
  },
  {
    name: "商品名称",
    key: "shopName"
  },
  {
    name: "数量",
    key: "total"
  }
];
const formatTable = ([{data}]) => {
  return data.slice(2).map((item) => {
    console.log(item);
    return item.reduce((pre, val, i) => {
      pre[clomus[i]['key']] = val;
      return pre
    },{})
  })
}
router.post('/upload', body({
  multipart:true
}), async (ctx) => {
  const { fileName } = ctx.request.body;
  const { file } = ctx.request.files;
  const newFileName = `${fileName}_${Date.now()}_${Math.round(Math.random() * 10000)}${file.originalFilename.match(/\.\w+$/)[0]}`
  // console.log(ctx.request.body, ctx.request.files, newFileName);
  // const readFile = fs.createReadStream(file.filepath);
  // const writeFile = fs.createWriteStream('./static/' + newFileName);
  // await saveFile(readFile,writeFile);
  const workSheetsFromBuffer = xlsx.parse(fs.readFileSync(file.filepath));
  // console.log();/
  
  ctx.body = {
    code: 200,
    status: 'success',
    filePath: '/static/'+newFileName,
    data:formatTable(workSheetsFromBuffer)
  }
})

app.listen(3000, function(){
  console.log('server port is 3000');
})