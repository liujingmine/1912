[
  {
    "role": "admin",
    "viewList": [
      {
        "path": '/view',
        "title": '访问统计'
      },
      {
        "path": '/user',
        "title": '用户管理'
      }
    ],
    "apiList": [
      {
        'path': '/view',
        'url': '/api/view/:id',
        'method': 'DELETE'
      }
    ]
  },
  {
    "role": "visitor",
    "viewList": [
      {
        "path": '/view',
        "title": '访问统计'
      }
    ],
    "apiList": [
      
    ]
  },
]